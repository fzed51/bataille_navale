var Ajax = function (url) {
    this.url = url;
    var xhttp = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhttp.onreadystatechange = (function (that) {
        return function () {
            if (this.readyState === 4 && this.status >= 200 && this.status < 400) {
                if (that.success_callback) {
                    that.success_callback(this.responseText);
                } else {
                    console.log('success');
                }
            }
            if (this.readyState === 4 && this.status >= 400) {
                if (that.error_callback) {
                    that.success_callback(this.responseText);
                } else {
                    console.log('error');
                }
            }
        };
    })(this);
    this.xhttp = xhttp;
    this.success_callback = null;
    this.error_callback = null;
};
Ajax.prototype.success = function (callback) {
    this.success_callback = callback;
    return this;
};
Ajax.prototype.error = function (callback) {
    this.error_callback = callback;
    return this;
};
Ajax.prototype.sendPost = function (data) {
    this.xhttp.open("POST", this.url, true);
    //this.xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //this.xhttp.setRequestHeader("Content-type", "multipart/form-data");
    this.xhttp.send(data);
};
Ajax.prototype.sendGet = function () {
    this.xhttp.open("GET", this.url, true);
    this.xhttp.send();
};
Ajax.prototype.send = function (data) {
    if (data) {
        Ajax.prototype.sendPost.call(this, data);
    } else {
        Ajax.prototype.sendGet.call(this);
    }
};