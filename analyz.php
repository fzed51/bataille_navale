<?php

include './log.php';
include './session.php';
include './config.php';
include './init_analyz.php';

$grille = new Grille(json_decode($_POST['grille']));

$navires_data = json_decode($_POST['navires']);
$navires = navires_non_coule($navires_data);

$nouvelle_grille = $grille->analyse($navires)->getGrille();

$string_out = json_encode($nouvelle_grille);

header('Content-Type: application/json; charset=UTF-8');
echo $string_out;
