<?php

include './Grille.php';
include './Cellule.php';
include './Navire.php';

function navires_non_coule($navires_data)
{
    $navires_data = (array) $navires_data;
    $config = [];
    foreach (config()->navires as $data) {
        $config[$data->nom] = $data->structure;
    }
    $out = [];
    foreach ($navires_data as $nom => $coulle) {
        if (!$coulle) {
            $out[] = new Navire($nom, $config[$nom]);
        }
    }
    return $out;
}

function MyJsonEncode($data, $option = [])
{

    $option = array_merge(
            [
        "STRING2HTML" => false
            ], $option);

    $is_assoc = function ($array) {
        foreach (array_keys($array) as $k => $v) {
            if ($k !== $v) {
                return true;
            }
        }
        return false;
    };

    switch (gettype($data)) {
        case "boolean":
            if ($data) {
                $out = ('true');
            } else {
                $out = ('false');
            }
            break;
        case "integer":
            $out = ("" . $data);
            break;
        case "double":
            $out = ("" . $data);
            break;
        case "string":
            if ($option['STRING2HTML']) {
                $out = ('"' . html($data) . '"');
            } else {
                $out = ('"' . $data . '"');
            }
            break;
        case "array":
            if ($is_assoc($data)) {
                $out = '{';
                $start = true;
                foreach ($data as $key => $value) {
                    if ($start) {
                        $start = false;
                    } else {
                        $out .= ',';
                    }
                    $out .= MyJsonEncode($key, ["STRING2HTML" => false]) . ':';
                    $out .= MyJsonEncode($value, $option);
                }
                $out .= '}';
            } else {
                $out = '[';
                $start = true;
                foreach ($data as $value) {
                    if ($start) {
                        $start = false;
                    } else {
                        $out .= ',';
                    }
                    $out .= MyJsonEncode($value, $option);
                }
                $out .= ']';
            }
            break;
        case "object":
            $obj = new ReflectionObject($data);
            $properties = $obj->getProperty(ReflectionProperty::IS_PUBLIC);
            $out = '{';
            $start = true;
            foreach ($properties as $property) {
                if ($start) {
                    $start = false;
                } else {
                    $out .= ',';
                }
                $out .= MyJsonEncode($property->name, ["STRING2HTML" => false]) . ':';
                $out .= MyJsonEncode($property->getValue($data), $option);
            }
            $out .= '}';
            break;
        case "NULL":
            $out = ('null');
            break;
        default :
            throw new RuntimeException("La donnée passé en paramètre de la fonction MyJsonEncode n'est pas valide");
    }
    return $out;
}
