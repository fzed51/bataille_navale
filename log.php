<?php

function logger($msg)
{
    if (php_sapi_name() == 'cli-server') {
        $fh = fopen('php://stderr', 'w');
    } else {
        $fh = fopen('.log.txt', 'a+');
    }
    $date = date(DATE_ATOM);
    fwrite($fh, "[$date]>$msg" . PHP_EOL);
    fclose($fh);
}
