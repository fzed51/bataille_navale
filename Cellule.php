<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cellule
 *
 * @author fabien.sanchez
 */
class Cellule
{

    private $val;
    private $y;
    private $x;

    public function __construct($x, $y, $val = 0)
    {
        $this->x = $x;
        $this->y = $y;
        $this->val = $val;
    }

    public function __get($key)
    {
        if (property_exists($this, $key)) {
            return $this->{$key};
        }
        throw new Exception("La propriété $key n'existe pas!");
    }

    public function estDecouverte()
    {
        if ($this->val < 0) {
            return true;
        }
        return false;
    }

    public function get()
    {
        return $this->val;
    }

    public function set($val)
    {
        $this->val = $val;
    }

}
