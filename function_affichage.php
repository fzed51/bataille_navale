<?php

function affiche_grille_colone($x, $y)
{
    for ($i = 0; $i < $x; $i++) {
        echo "<td data-x=\"$i\" data-y=\"$y\" class=\"js_x{$i}y{$y}\" >0</td>" . PHP_EOL;
    }
}

function affiche_grille_ligne($x, $y)
{
    for ($i = 0; $i < $y; $i++) {
        echo '<tr>';
        affiche_grille_colone($x, $i);
        echo '</tr>' . PHP_EOL;
    }
}

function affiche_grille()
{
    list($x, $y) = config()->grille;
    echo '<table>';
    affiche_grille_ligne($x, $y);
    echo '</table>' . PHP_EOL;
}

function affiche_navire($nom)
{
    echo '<div>';
    echo '<label><input type="checkbox" name="navires[]" value="' . $nom . '">' . $nom . '</label>';
    echo '</div>' . PHP_EOL;
}

function affiche_navires()
{
    $navires = config()->navires;
    foreach ($navires as $navire) {
        affiche_navire($navire->nom);
    }
}
