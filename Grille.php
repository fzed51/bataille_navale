<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grille
 *
 * @author fabien.sanchez
 */
class Grille
{

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function setAt($x, $y, $val)
    {
        if (!isset($this->data[$x][$y])) {
            $this->redim($x, $y);
        }
        $this->data[$x][$y] = $val;
    }

    private function redim($x, $y)
    {
        while (!isset($this->data[$x])) {
            $this->data[] = [];
        }
        for ($x = 0; $x < count($this->data); $x++) {
            while (!isset($this->data[$x][$y])) {
                $this->data[$x][] = 0;
            }
        }
    }

    public function set(Cellule $cell)
    {
        $this->setAt($cell->x, $cell->y, $cell->val);
    }

    public function getAt($x, $y)
    {
        if (isset($this->data[$x][$y])) {
            return $this->data[$x][$y];
        }
        return 0;
    }

    public function addAt($x, $y, $val)
    {
        if (!isset($this->data[$x][$y])) {
            $this->redim($x, $y);
        }
        $this->data[$x][$y] += $val;
    }

    public function add(Cellule $cell)
    {
        $this->addAt($cell->x, $cell->y, $cell->val);
    }

    /**
     * @return Cellule
     */
    private function cellules()
    {
        for ($x = 0; $x < count($this->data); $x++) {
            for ($y = 0; $y < count($this->data[$x]); $y++) {
                yield new Cellule($x, $y, $this->data[$x][$y]);
            }
        }
    }

    private function nombreNavireSurCellule($navires, $cellule)
    {
        $count = 0;
        foreach ($navires as $navire) {
            $orientations = $navire->orientations();
            foreach ($orientations as $orientation) {
                $count += $this->nombrePositionNavireSurCellule($orientation, $cellule);
            }
        }
        return $count;
    }

    private function nombrePositionNavireSurCellule($navire, $cellule)
    {
        $count = 0;
        $sub_structures = $navire->subStructures();
        foreach ($sub_structures as $sub_structure) {
            if ($this->peuPlacerNavireSur($navire, $cellule->x - $sub_structure[0], $cellule->y - $sub_structure[1])) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * @param Navire $navire
     * @param int $x
     * @param int $y
     * @return boolean
     */
    private function peuPlacerNavireSur($navire, $x, $y)
    {
        $sub_structures = $navire->subStructures();
        foreach ($sub_structures as $sub_structure) {
            $_x = $x + $sub_structure[0];
            $_y = $y + $sub_structure[1];
            if ($_x < 0) {
                return false;
            }
            if ($_y < 0) {
                return false;
            }
            if ($_x >= count($this->data)) {
                return false;
            }
            if ($_y >= count($this->data[0])) {
                return false;
            }
            if ($this->data[$_x][$_y] < 0) {
                return false;
            }
        }
        return true;
    }

    public function analyse(array $navires)
    {
        $index = 0;
        $nouvelle_grille = new Grille([]);
        /* @var $navire Navire */
        foreach ($navires as $navire) {
            logger("Debut navire " . ++$index);
            $tmp = $this->getPosibiliteNavire($navire);
            $nouvelle_grille->merge($tmp);
            unset($tmp);
            gc_collect_cycles();
            logger("Fin navire " . $index);
        }
        $nouvelle_grille->repportCellulesDecouvertes($this);
        return $nouvelle_grille;
    }

    public function getGrille()
    {
        return $this->data;
    }

    public function merge(Grille $grille)
    {
        $merge_cells = $grille->cellules();
        foreach ($merge_cells as $cell) {
            $local = $this->add($cell);
        }
    }

    public function getPosibiliteNavire(Navire $navire): Grille
    {
        $orientations = $navire->orientations();
        $nouvelle_grille = new Grille([]);
        foreach ($orientations as $orientation) {
            $tmp = $this->getPosibiliteNavireOrientation($orientation);
            $nouvelle_grille->merge($tmp);
            unset($tmp);
            gc_collect_cycles();
        }
        return $nouvelle_grille;
    }

    public function getPosibiliteNavireOrientation(Navire $orientation): Grille
    {
        $nouvelle_grille = new Grille([]);
        /* @var $cellules Generator */
        $cellules = $this->cellules();
        foreach ($cellules as $cellule) {
            if ($this->peuPlacerNavireSur($orientation, $cellule->x, $cellule->y)) {
                $nouvelle_grille->placeNavire($cellule->x, $cellule->y, $orientation);
            }
        }
        return $nouvelle_grille;
    }

    public function placeNavire(int $x, int $y, Navire $navire)
    {
        $sub_structures = $navire->subStructures();
        foreach ($sub_structures as $sub_structure) {
            $this->addAt($x + $sub_structure[0], $y + $sub_structure[1], 1);
        }
    }

    public function repportCellulesDecouvertes($grille)
    {
        $cellules = $grille->cellules();
        foreach ($cellules as $cell) {
            if ($cell->estDecouverte()) {
                $this->set($cell);
            }
        }
    }

}
