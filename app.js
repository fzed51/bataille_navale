/* global grille */

+(function () {

    var getNavires = function () {
        var navires = {};
        var inputs = document.querySelectorAll('#navires input');
        for (var i = 0; i < inputs.length; i++) {
            var input = inputs[i];
            navires[input.value] = input.checked;
        }
        return navires;
    };

    var affiche_analyz = function (data) {
        console.log('recieve :');
        console.log(data);
        try {
            var best = [0, 0, 0],
                    essais = 0,
                    json = JSON.parse(data);
            grille = json;
            for (var x = 0; x < json.length; x++) {
                for (var y = 0; y < json[x].length; y++) {
                    var cell = document.querySelector('.js_x' + x + 'y' + y);
                    if (json[x][y] > best[2]) {
                        best = [x, y, json[x][y]];
                    }
                    if (cell) {
                        cell.innerHTML = json[x][y];
                        if (json[x][y] == -1) {
                            essais++;
                            cell.className = "shoot " + 'js_x' + x + 'y' + y;
                        } else if (json[x][y] == -2) {
                            essais++;
                            cell.className = "ship " + 'js_x' + x + 'y' + y;
                        } else {
                            cell.className = 'js_x' + x + 'y' + y;
                        }
                    }
                }
            }
            var cell = document.querySelector('.js_x' + best[0] + 'y' + best[1]);
            cell.classList.add('best');
            var cell = document.querySelector('#essais');
            cell.innerHTML = essais;
        } catch (e) {
            var div_er = document.querySelector('#error');
            if (!div_er) {
                div_er = document.createElement('div');
                div_er.id = 'error';
                document.body.appendChild(div_er);
            }
            div_er.innerHTML = data;
        }
    };

    var click_analyz = function (e) {
        var v = grille[e.target.dataset.x][e.target.dataset.y];
        if (v > 0) {
            v = 0;
        }
        v = ((v - 1) % 3);
        e.target.innerHTML = v;
        grille[e.target.dataset.x][e.target.dataset.y] = v;
        setTimeout(analyz, 2, e);
    }

    var analyz = function () {
        var request = (new Ajax('analyz.php')).success(affiche_analyz);
        var data = new FormData();
        console.log('send :');
        console.log(grille);
        console.log(getNavires());
        data.append('grille', JSON.stringify(grille));
        data.append('navires', JSON.stringify(getNavires()));
        request.send(data);
    };

    var initEventListener = function () {
        var cells = document.querySelectorAll('#grille td');
        var inputs = document.querySelectorAll('#navires input');
        for (var i = 0; i < cells.length; i++) {
            var cell = cells[i];
            cell.addEventListener('click', click_analyz);
        }
        for (var i = 0; i < inputs.length; i++) {
            var input = inputs[i];
            input.addEventListener('change', analyz);
        }
    };

    document.addEventListener('DOMContentLoaded', initEventListener);
})();