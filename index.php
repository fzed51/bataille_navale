<?php
include 'init_bataille_navale.php';
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bataille navale -- Helper</title>
        <script src="ajax.js" type="application/javascript" ></script>
        <script src="helper.js" type="application/javascript" ></script>
        <script src="app.js" type="application/javascript" ></script>
        <link rel="stylesheet" href="./app.css" />
    </head>
    <body>
        <div id="grille">
            <?= affiche_grille(); ?>
        </div>
        <div id="navires">
            <?= affiche_navires(); ?>
        </div>
        <div id="essais">
            0
        </div>
        <button id="analyz">Lancer l'analyze</button>
        <script type="application/javascript">
            var grille = (function (x, y) {
            var out = [];
            for(var j = 0; j < y; j++){
            out[j] = Array(x).fill(0)
            }
            return out;
            })(<?= config()->grille[0]; ?>, <?= config()->grille[1]; ?>);
        </script>
    </body>
</html>
