<?php

function config()
{
    if (!isset($_SESSION['config'])) {
        $_SESSION['config'] = json_decode(file_get_contents('./config.json'));
    }
    return $_SESSION['config'];
}
