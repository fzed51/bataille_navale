<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Navire
 *
 * @author fabien.sanchez
 */
class Navire
{

    private $structure;
    private $nom;

    static private function is_2DimentionalArray($array)
    {
        return array_reduce(
                $array, function($test, $data) {
            return $test && is_array($data);
        }, true);
    }

    static private function rotateArray($array)
    {
        $out = [];
        for ($i = 0; $i < count($array[0]); $i++) {
            $out[] = [];
        }
        for ($i = 0; $i < count($array); $i++) {
            for ($j = 0; $j < count($array[$i]); $j++) {
                $jj = count($array[$i]) - 1 - $j;
                $out[$jj][$i] = $array[$i][$j];
            }
        }
        return $out;
    }

    public function __construct($nom, $structure)
    {
        $this->nom = $nom;
        if (!self::is_2DimentionalArray($structure)) {
            throw new Exception('La structure du navire doit être un tableau à 2 dimention! ');
        }
        $this->structure = $structure;
    }

    private function orientation($nb)
    {
        $nb %= 4;
        if ($nb < 0) {
            $nb += 4;
            $nb %= 4;
        }
        $structure = $this->structure;
        for ($i = 0; $i < $nb; $i++) {
            $structure = self::rotateArray($structure);
        }
        return new self($this->nom, $structure);
    }

    public function orientations()
    {
        for ($i = 0; $i < 4; $i++) {
            yield $this->orientation($i);
        }
    }

    public function subStructures()
    {
        for ($x = 0; $x < count($this->structure); $x++) {
            for ($y = 0; $y < count($this->structure[$x]); $y++) {
                if ($this->structure[$x][$y] > 0) {
                    yield [$x, $y];
                }
            }
        }
    }

}
